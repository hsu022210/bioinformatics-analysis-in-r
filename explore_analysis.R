d <- read.csv("mom_baby_PKU2.csv", stringsAsFactors = F, na.strings=c("?", -999))
sapply(d, is.numeric)

library("ggplot2")
g <- ggplot(data=d, aes(x=age))
g + geom_histogram(binwidth = 0.5)

g <- ggplot(data=d, aes(x=mVIQ))
g + geom_histogram(binwidth = 1.5)

g <- ggplot(data=d, aes(x=mPIQ))
g + geom_histogram(binwidth = 1.5)

g <- ggplot(data=d, aes(x=mIQ))
g + geom_histogram(binwidth = 1)

g <- ggplot(data=d, aes(x=wt_gain))
g + geom_histogram(binwidth = 1.5)

g <- ggplot(data=d, aes(x=phe_sd))
g + geom_histogram(binwidth = 5)

g <- ggplot(data=d, aes(x=phe_avg))
g + geom_histogram(binwidth = 30)

g <- ggplot(data=d, aes(x=gestation))
g + geom_histogram(binwidth = 0.3)

g <- ggplot(data=d, aes(x=birth_len))
g + geom_histogram(binwidth = 0.5)

g <- ggplot(data=d, aes(x=birth_wt))
g + geom_histogram(binwidth = 50)

g <- ggplot(data=d, aes(x=head_circum))
g + geom_histogram(binwidth = 0.3)

g <- ggplot(data=d, aes(x=MDI))
g + geom_histogram(binwidth = 1)

g <- ggplot(data=d, aes(x=PDI))
g + geom_histogram(binwidth = 1)

cor(d, use="complete.obs")

fit1 <- lm(MDI ~ phe_avg, data = d)
summary(fit1)
plot(fit1, 1)
# The residuals in summary for the model are the weighted residuals if weights were given in the model.
# The F-statistics in summary for the model is a numeric vector of length three giving the F-test for the regression. The first element in the vector is the statistic, and the last two elements are the degrees of freedom.
# for adjusted R^2, the best result is 1, which means 100% percent correct on predicting, otherwise then larger the better.

fit1 <- lm(PDI ~ phe_avg, data = d)
summary(fit1)
plot(fit1, 1)

fit1 <- lm(birth_wt ~ wt_gain*gestation*head_circum*birth_len, data = d)
summary(fit1)
plot(fit1, 1)
